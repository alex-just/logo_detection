#!/usr/bin/env python3

"""
Installation:
    $ pip3 install requests

Example usage:
    $ python3 client.py

Example output:
    {"probabilty": 0.8, "size": 0.3, "boxlocation": [133, 142, 245, 243]}
"""

from os import path

import requests


class BrandDetection(object):
    def APICALL(self, logo_name, image=None):
        url = 'http://108.61.220.218:9000/detect'

        data = {
            'logo_name': logo_name,
            'image': image,
        }

        r = requests.post(url, data=data)
        return r.text


if __name__ == '__main__':
    image = None

    image_path = 'test.png'
    if path.isfile(image_path):
        with open(image_path, 'rb') as f:
            image = f.read()

    logo_name = 'Heineken'

    branddetection = BrandDetection()

    output = branddetection.APICALL(logo_name, image)
    print(output)
