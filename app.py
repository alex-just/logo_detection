#!/usr/bin/env python3

"""
Installation:
    $ pip3 install -r requirements.txt

Example usage:
1) start server
    $ gunicorn gunicorn --bind 0.0.0.0:9000 wsgi:app

2.1) query started server via OS tools
    $ curl -i -X POST -H "Content-Type: multipart/form-data" -F "logo_name=Heineken" localhost:9000/detect
    or
    $ curl -i -X POST -H "Content-Type: multipart/form-data" -F "logo_name=Heineken" -F "image=@test.png" localhost:9000/detect
    or
    $ http -f POST localhost:9000/detect logo_name='Heineken'
    or
    $ http -f POST localhost:9000/detect logo_name='Heineken' image@test.png

2.2) query started server via python client lib
    $ python3 client.py

Example output:
    {"probabilty": 0.8, "size": 0.3, "boxlocation": [133, 142, 245, 243]}
"""

# IMPORT _______________________________________________________________________________________________________________
import io
import json
import sys
from os import path
from wsgiref import simple_server

import falcon
from werkzeug.wrappers import Request

# GLOBALS ______________________________________________________________________________________________________________
DEBUG = sys.gettrace() is not None
CONFIG = {
    'detection_module_path': None,
    'images_folder_path': None,
}
DETECTION_MODULE = None

# falcon.API instances are callable WSGI apps
app = falcon.API()


# UTILS ________________________________________________________________________________________________________________
def detect_brand(logo_name, image=None):
    print('API | detect_brand(logo_name = "{}", image)'.format(logo_name))
    uploaded_image_filename = 'uploaded'

    if image:
        save_image_on_disk(CONFIG['images_folder_path'], uploaded_image_filename, image)

    from mainalgo import apifunc

    print('API | mainalgo.apifunc.detect(image_file_name = "{}", logoname = "{}", logonum_version_name = "21")'
          .format('testimageapi2.jpg', logo_name))

    response = apifunc.detect(uploaded_image_filename + '.jpg', logo_name)
    print('API | response = ' + str(response) + ' type is ' + str(type(response)))
    return str(response)


def save_image_on_disk(upload_folder, logo_name, image):
    filename = '{}.jpg'.format(logo_name)
    image_path = path.join(upload_folder, filename)

    with open(image_path, 'wb') as f:
        f.write(image.read())

    print('API | save_image_on_disk(upload_folder = "{}", logo_name = "{}", image)'.format(upload_folder, logo_name))


def read_config_from_file():
    global CONFIG, DETECTION_MODULE

    try:
        with open('config.json') as f:
            CONFIG = json.load(f)

        assert CONFIG['detection_module_path'] and CONFIG['images_folder_path']

    except Exception as ex:
        raise ValueError(
            'Error! config.json file must exist and contain both '
            '`detection_module_path` and `images_folder_path` values. ' + str(ex))

    # Add detection module to the sys.path
    import sys
    sys.path.append(CONFIG['detection_module_path'])

    # Test it
    try:
        from mainalgo import apifunc
    except Exception as ex:
        raise ValueError('Error! Cannot load detection module {}\n'.format(CONFIG['detection_module_path']) + str(ex))


# API ENDPOINT HANDLERS ________________________________________________________________________________________________
class IndexResource(object):
    def on_get(self, req, resp):
        resp.body = 'Web service is up and running!'


class DetectResource(object):
    def on_post(self, req, resp):
        werkzeug_request = Request(req.env)
        image = None

        # Get image from request
        try:
            file_stream = werkzeug_request.files.get('image')
            if file_stream:
                image = io.BytesIO(file_stream.stream.read())

        except Exception as ex:
            raise falcon.HTTPBadRequest('Error processing `image` input parameter', str(ex))

        # Get logo_name from request
        logo_name = werkzeug_request.form.get('logo_name', None)
        if not logo_name:
            raise falcon.HTTPBadRequest('Missing parameter', 'A `logo_name` must be submitted in the request body.')

        # Return response
        response = detect_brand(logo_name, image)
        resp.body = response


# ROUTES _______________________________________________________________________________________________________________
app.add_route('/', IndexResource())
app.add_route('/detect', DetectResource())

# MAIN _________________________________________________________________________________________________________________
if __name__ == '__main__':
    read_config_from_file()

    print('API | Serving on http://127.0.0.1:9000...')
    httpd = simple_server.make_server('127.0.0.1', 9000, app)
    httpd.serve_forever()
